#!/bin/bash -v
echo "Running agent boot script \n"

tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1
wget -O "$tempdeb" https://apt.puppetlabs.com/puppet6-release-bionic.deb
sudo dpkg -i "$tempdeb"
sudo apt-get update
sudo apt-get -y install puppet-agent
 # add Puppet binaries to PATH:
 echo 'export PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
 # source .bashrc to apply:
 . ~/.bashrc
echo "$(/opt/puppetlabs/bin/facter networking.ip) $(hostname).beauvine.vm $(hostname)" >> /etc/hosts
echo "manager_ip_address manager.puppetlabs.vm manager" >> /etc/hosts
/opt/puppetlabs/bin/puppet config set server manager.puppetlabs.vm --section main
/opt/puppetlabs/bin/puppet config set runinterval 300 --section main
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
sleep 4m
sudo -i puppet agent -t
sleep 30s

sudo -i echo "# ************************************
# Vhost template in module puppetlabs-apache
# Managed by Puppet
# ************************************
<VirtualHost *:80>
  ServerName default

  ## Vhost docroot
  DocumentRoot '/var/www/'

  ## Directories, there should at least be a declaration for /var/www/html

  <Directory '/var/www/'>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
  </Directory>

  ## Logging
  ErrorLog '/var/log/apache2/default-80_error.log'
  ServerSignature Off
  CustomLog '/var/log/apache2/access.log' combined
  ## Script alias directives
  ScriptAlias /cgi-bin '/usr/lib/cgi-bin'
</VirtualHost>" > /etc/apache2/sites-available/15-default-80.conf

service apache2 restart