#!/bin/bash
echo "Running manager boot script \n"

#Enable the Puppet platform (with Apt):
wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
sudo dpkg -i puppet6-release-bionic.deb

sudo apt-get update
sudo apt-get install puppetserver facter r10k -y
sudo apt-get update
echo 'export PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
source ~/.bashrc

echo "$(facter networking.ip) $(hostname).puppetlabs.vm $(hostname)" | sudo tee -a /etc/hosts
echo "$(facter networking.ip) puppet" | sudo tee -a /etc/hosts

sudo -i puppetserver ca setup
sudo -i puppet resource service puppetserver ensure=running enable=true
echo "agent.openstacklocal" | sudo tee -a /etc/puppetlabs/puppet/autosign.conf

/opt/puppetlabs/bin/puppet config set server manager.puppetlabs.vm --section main
/opt/puppetlabs/bin/puppet config set runinterval 300 --section main
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

sleep 30s
sudo -i puppetserver ca sign --all
sudo -i git clone https://gitlab.stud.idi.ntnu.no/madsdh/iac-modul.git /tmp/wordpress

sleep 30s

sudo -i puppet module install puppetlabs-apache
sudo -i puppet module install puppetlabs-mysql
sudo -i puppet module install puppetlabs-concat
sudo -i puppet module install /tmp/wordpress/ubuntu-wordpress-0.1.0.tar.gz

sudo -i puppet module list

echo "class { 'wordpress':
}" >> /etc/puppetlabs/code/environments/production/manifests/install-wp.pp

echo "Module running on floating ip"